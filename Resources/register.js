var homeNav = require("homescreen").homeNav;
var emailIsValid = require("login").emailIsValid;

regWin = Ti.UI.createWindow({
	backgroundImage: "register.jpg",
	title: "Register"
});
var openLabel = Ti.UI.createLabel({
	text: "Please Enter Your Information Below",
	color: "white",
	top: 30
});

var firstField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  color: 'gray',
  opacity: .8,
  hintText: "Enter First Name",
  top: 100, 
  width: 300, 
  height: 30,
});

var lastField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  color: 'gray',
  opacity:.8,
  hintText: "Enter Email",
  top: 175, 
  width: 300, 
  height: 30,
});

var userField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  color: 'gray',
  opacity:.8,
  hintText: "Enter Password",
  passwordMask: true,
  top: 250, 
  width: 300, 
  height: 30,
});

var passField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  color: 'gray',
  opacity:.8,
  hintText: "Re-enter Password",
  passwordMask: true,
  top: 325, 
  width: 300, 
  height: 30,
});

var loginButton = Ti.UI.createView({
	backgroundColor: "white",
	opacity: .9,
	top: 430,
	width: 200,
	height: 50
});

var logLabel = Ti.UI.createLabel({
	text: "Create Account"
});

var error = Ti.UI.createLabel({
	text : "One or more fields incorrect, try again.",
	color : "red",
	top : 55, 
	visible: false,
});

loginButton.add(logLabel);

loginButton.addEventListener("click", function(){
	console.log("Password: " + passField.getValue());
	console.log("Password Confirm:" + userField.getValue());
	console.log("Email:" + emailField.getValue());
	
	if (emailIsValid(lastField.getValue()) && passField.getValue() == userField.getValue()) {
		homeNav.open();
		firstField.setValue("");
		lastField.setValue("");
		userField.setValue(" ");
		passField.setValue("");
		firstField.setBorderColor("white");
		lastField.setBorderColor("white");
		userField.setBorderColor("white");
		passField.setBorderColor("white");
		error.setVisible("false");
	} else {
		console.log(emailIsValid(emailField.getValue()));
		console.log(passField.getValue());
		console.log(userField.getValue());
		
		firstField.setBorderColor("red");
		lastField.setBorderColor("red");
		userField.setBorderColor("red");
		passField.setBorderColor("red");
		error.setVisible("true");
	}
});

	

regWin.add(openLabel, firstField, lastField, userField, passField, loginButton, error);
exports.regWin = regWin;

