var parkData = require("data").parkData;

var getDetail = function(parkData){
	var detailWin = Ti.UI.createWindow({
		backgroundImage: "background.jpg",
		title: parkData.title,
	});
	
	var detailText = Ti.UI.createLabel({
		text: parkData.title,
		top: 60,
		textAlign: 'center'
	});
	
	var detailDate = Ti.UI.createLabel({
		text: parkData.date,
		bottom: 100,
	});
	
	var detailSymbol = Ti.UI.createLabel({
		text:parkData.symbol,
		bottom: 50
	});
	
	var detailImage = Ti.UI.createImageView({
		image: parkData.image,
		height: 200,
		width: 200,
		top:100
	});
	
	var view = Ti.UI.createView({
		backgroundColor: "white"
	});
	

	detailWin.add(detailDate, detailSymbol, detailImage);

	return detailWin;
};


exports.getDetail = getDetail;