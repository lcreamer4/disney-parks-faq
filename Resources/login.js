//Function to check that email is valid
function emailIsValid(email) {
	//Store Position of @ symbol. Default to -1 or NONE
	var atPos = -1;
	
	if (
		// If there is an @
		email.indexOf("@") != -1
		
		//AND the @ is NOT the last item of the string
		&& email.indexOf("@") != email.length - 1) {
			
			//Set position of the @ symbol
			atPos = email.indexOf("@");
			
			//IF there is a period that is not next to the @
			if(email.indexOf(".") > atPos + 1
			//IF there is a period that is not next to the @
			&& email.indexOf(".") < email.length - 2) {
				// Email is valid
				return true;
			} else {
				
				//Email is not valid
				return false;
			}
		} else {
			//Email is not valid
			return false;
		
	}
}

exports.emailIsValid = emailIsValid;
