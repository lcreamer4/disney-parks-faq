var regWin = require("register").regWin;
var homeNav = require("homescreen").homeNav;
var emailIsValid = require("login").emailIsValid;

var win = Ti.UI.createWindow({
	backgroundImage : "register.jpg",
	title : "Login"
});

var welcomeLabel = Ti.UI.createLabel({
	text : "Welcome to the Kingdom!",
	textAlign : "center",
	color : "white",
	top : 30,

});

var emailField = Ti.UI.createTextField({
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
	color : 'gray',
	opacity : .8,
	hintText : "Enter Email",
	top : 300,
	width : 300,
	height : 30,
});

var passwordField = Ti.UI.createTextField({
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
	color : 'gray',
	opacity : .8,
	hintText : "Enter Password",
	top : 375,
	width : 300,
	height : 30,
	passwordMask : true
});

var login = Ti.UI.createView({
	backgroundColor : "white",
	opacity : .8,
	top : 450,
	left : 20,
	width : 100,
	height : 25
});

var regButton = Ti.UI.createView({
	backgroundColor : "white",
	opacity : .8,
	top : 450,
	right : 20,
	width : 150,
	height : 25
});

var regLabel = Ti.UI.createLabel({
	text : "Register"
});

var loginLable = Ti.UI.createLabel({
	text : "Login"
});

regButton.add(regLabel);
login.add(loginLable);


var error = Ti.UI.createLabel({
	text : "Email or password incorrect, try again.",
	color : "red",
	top : 270, 
	visible: false,
});

win.add(welcomeLabel, emailField, passwordField, login, regButton, error);

var loginNav = Ti.UI.iOS.createNavigationWindow({
	window : win
});



regButton.addEventListener("click", function() {
	loginNav.openWindow(regWin);
});

login.addEventListener("click", function() {
	if (emailIsValid(emailField.getValue()) && passwordField.getValue() <5) {
		homeNav.open();
		emailField.setValue("");
		passwordField.setValue("");
		emailField.setBorderColor("white");
		passwordField.setBorderColor("white");
		error.setVisible("false");
	} else {
		emailField.setBorderColor("red");
		passwordField.setBorderColor("red");
		error.setVisible("true");
	}
});

loginNav.open(); 