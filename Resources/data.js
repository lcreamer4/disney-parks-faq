exports.parkData = [{
	title : "Magic Kingdom",
	date : "Opened on October 1, 1971 ", 
	symbol: "Iconic Symbol: Cinderella Castle",
	image: "castle.jpg",
	
	
}, {
	title : "Epcot",
	date: "Opened on October 1, 1982 ",
	symbol: "Iconic Symbol: Spaceship Earth",
	image: "epcot.jpg"
}, {
	title : "Hollywood Studios",
	date: "Opened on May 1, 1989 ",
	symbol: "Iconic Symbol: Sorcerers Hat ",
	image: "hollywoodstudios.jpeg"
	
}, {
	title : "Animal Kingdom",
	date : "Opened on April 22, 1998 ",
	symbol: "Iconic Symbol: The Tree of Life",
	image: "tree.jpg"
	
}, {
	title : "Typhoon Lagoon",
	date : "Opened on June 1, 1989 ",
	symbol: "Iconic Symbol: Miss Tilly",
	image: "typhoon.jpg"
	
}, {
	title : "Blizzard Beach",
	date: "Opened on April 1, 1995",
	symbol: "Iconic Symbol: Summit Plummit",
	image: "blizzard.jpeg"
	
}, {
	title : "Disney Springs",
	date : "Opened on March 22, 1975",
	symbol: "Iconic Symbols: Multiple",
	image: "downtown.jpg"
	
}, {
	title : "Resorts",
	date : "There are over 25 Disney resorts.",
	symbol: "Origional Hotels: The Polynesian, The Contemporary",
	image: "resort.jpg"
	
}, {
	title : "Transportation",
	date : "Buses, trains, and boats",
	symbol: "Complimary Transpotation throughout the resort.",
	image: "transport.jpg"
	
}, {
	title : "Tips",
	date : "Come early when it is cooler and leave around lunch, then return in the evening",
	symbol: "Bring water and snacks to cut down on costs for food and drinks in the parks",
	image: "tips.jpg"
	
}, {
	title : "Golf Courses",
	date : "There are 4 golf courses on Disney property. ",
	symbol: "The courses are run by PGA professional cast members",
	image: "golf.jpg"
	
}, {
	title : "Disney Quest",
	date : "Disney Quest is an indoor interactive theme park in Disney Springs.",
	symbol: "Opened in June 19 1998",
	image: "quest.jpg"

}, {
	title : "Bowling and the Movies",
	date : "For some more indoor fun visit AMC theatres and Splitsville in Disney Springs.",
	symbol: "Opened December 10, 2013",
	image: "bowling.jpg"	
	
	}, {
	title : "Other Activites",
	date : "Many of the resorts offer poolside activities as well as evening activities.",
	symbol: "S'mores, Movies Under the Stars, Pirates Cruise, Bike Rentals, Horse Drawn Caraiges",
	image: "bike.jpeg"	
}
];
