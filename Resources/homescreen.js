var parkData = require("data").parkData;
var detail = require("detail").getDetail;
var search = require("search").search;
var win = require("app").win;

var logoutButton = Ti.UI.createButton({
	title: "Log Out"
});


// Create Window
var tableWin = Ti.UI.createWindow({
	backgroundColor : "white",
	title : "Disney FAQ",
	rightNavButton: logoutButton
});

// Create Table Data Empty Array
var tableData = [];

var table = Ti.UI.createTableView({
	data : tableData,
	top : 20,
	search:search,
	editable: true
});

// Loop through data
for (var i = 0, j = parkData.length; i < j; i++) {
	var park = parkData[i];

	// For EACH character create a row
	var row = Ti.UI.createTableViewRow({
		backgroundImage: "background.jpg",
		title : park.title,
		editable: true,
		data : park,
		
	});

	// Push the row to the table data
	tableData.push(park);
	// Set table data
	table.setData(tableData);
};
var homeNav = Ti.UI.iOS.createNavigationWindow({
	window : tableWin
});

logoutButton.addEventListener("click", function(){
	  var dialog = Ti.UI.createAlertDialog({
    cancel: 1,
    buttonNames: ['Log Out', 'Cancel'],
    message: 'Are you sure you want to log out?',
    title: 'Log Out'
  });
  dialog.addEventListener('click', function(e){
    if (e.index === e.source.cancel){
      homeNav.open();
    } else {
    	homeNav.close();
    }
   
  });
  dialog.show();
});

// Click Event ON the table
table.addEventListener("click", function(e) {
	var data = e.source;
	
	var detailsWindow = detail(data);
	
	homeNav.openWindow(detailsWindow);
});

// Add table to the window
tableWin.add(table);

exports.homeNav = homeNav;
